<tr>
    <td><a href="https://youtube.com/watch?v={!! $id !!}"><img src="{!! $snippet->thumbnails->medium->url !!}" width="{!! $snippet->thumbnails->medium->width !!}" height="{!! $snippet->thumbnails->medium->height !!}"></a></td>
    <td style="vertical-align: top;">
    	<a href="https://youtube.com/watch?v={!! $id !!}"><h3>{{ $snippet->title }}</h3></a>
    	<span>{{ $snippet->description }}</span>
    </td>
</tr>