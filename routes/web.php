<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;

Route::get('/', function (Request $request) {
    $data = [];
    if (!empty($youser = $request->input('youser'))) {
        $data['youser'] = $youser;
        if (($channel = Youtube::getChannelByName($youser)) !== false) {
            $data['channel'] = $channel;
            if ($videos = Youtube::listChannelVideos($channel->id, 40)) {
	            $data['channel']->videos = $videos;
			}
        } else {
            $data['error'] = true;
        }
    } else {
    	$data['youser'] = '';
    }
    return view('youtube', $data);
});
